import 'package:desafio_tecnico/app/shared/models/auth_model.dart';

class AppController {
  final AuthModel authModel = AuthModel(accessToken: '', client: '', uid: '');
}
