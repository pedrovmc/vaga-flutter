import 'package:desafio_tecnico/app/app_controller.dart';
import 'package:desafio_tecnico/app/modules/list_companies/list_companies_module.dart';
import 'package:desafio_tecnico/app/modules/login/login_module.dart';
import 'package:desafio_tecnico/app/shared/models/auth_model.dart';
import 'package:desafio_tecnico/app/splash_screen_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
    Bind((i) => AppController()),
    Bind((i) => AuthModel(client: '', accessToken: '', uid: '')),
    Bind((i) => Dio(BaseOptions(
        baseUrl: 'https://empresas.ioasys.com.br/api/v1/',
        connectTimeout: 5000))),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, args) => SplashScreenPage()),
    ModuleRoute('/login', module: LoginModule()),
    ModuleRoute('/list-companies', module: ListCompaniesModule())
  ];
}
