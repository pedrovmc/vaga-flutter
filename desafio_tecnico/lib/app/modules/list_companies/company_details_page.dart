import 'package:desafio_tecnico/app/modules/list_companies/widgets/company_list_item.dart';
import 'package:desafio_tecnico/app/shared/models/company_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CompanyDetailsPage extends StatelessWidget {
  final CompanyModel companyModel;
  final Color color;
  const CompanyDetailsPage({
    required this.companyModel,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 92,
          leading: IconButton(
            onPressed: () {
              Modular.to.pop();
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Color(0xFFE01E69),
            ),
          ),
          title: Text(
            companyModel.name,
            style: const TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              CompanyListItem(
                companyName: companyModel.name,
                color: color,
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  companyModel.description,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
        ));
  }
}
