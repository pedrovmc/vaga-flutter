import 'package:desafio_tecnico/app/shared/models/company_model.dart';
import 'package:desafio_tecnico/app/shared/repositories/companies_repository.dart';
import 'package:mobx/mobx.dart';

part 'list_companies_controller.g.dart';

class ListCompaniesController = _ListCompaniesControllerBase
    with _$ListCompaniesController;

abstract class _ListCompaniesControllerBase with Store {
  final CompaniesRepository companiesRepository;
  _ListCompaniesControllerBase({
    required this.companiesRepository,
  });

  @observable
  List<CompanyModel> listedCompanies = [];

  @observable
  bool initialState = true;

  @observable
  bool busy = false;

  @action
  Future<void> searchByName(String name) async {
    initialState = false;
    if (name != '') {
      busy = true;
      listedCompanies = await companiesRepository.getAllByName(name);
      busy = false;
    } else {
      listedCompanies = [];
    }
  }
}
