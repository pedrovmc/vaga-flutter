// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_companies_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ListCompaniesController on _ListCompaniesControllerBase, Store {
  final _$listedCompaniesAtom =
      Atom(name: '_ListCompaniesControllerBase.listedCompanies');

  @override
  List<CompanyModel> get listedCompanies {
    _$listedCompaniesAtom.reportRead();
    return super.listedCompanies;
  }

  @override
  set listedCompanies(List<CompanyModel> value) {
    _$listedCompaniesAtom.reportWrite(value, super.listedCompanies, () {
      super.listedCompanies = value;
    });
  }

  final _$initialStateAtom =
      Atom(name: '_ListCompaniesControllerBase.initialState');

  @override
  bool get initialState {
    _$initialStateAtom.reportRead();
    return super.initialState;
  }

  @override
  set initialState(bool value) {
    _$initialStateAtom.reportWrite(value, super.initialState, () {
      super.initialState = value;
    });
  }

  final _$busyAtom = Atom(name: '_ListCompaniesControllerBase.busy');

  @override
  bool get busy {
    _$busyAtom.reportRead();
    return super.busy;
  }

  @override
  set busy(bool value) {
    _$busyAtom.reportWrite(value, super.busy, () {
      super.busy = value;
    });
  }

  final _$searchByNameAsyncAction =
      AsyncAction('_ListCompaniesControllerBase.searchByName');

  @override
  Future<void> searchByName(String name) {
    return _$searchByNameAsyncAction.run(() => super.searchByName(name));
  }

  @override
  String toString() {
    return '''
listedCompanies: ${listedCompanies},
initialState: ${initialState},
busy: ${busy}
    ''';
  }
}
