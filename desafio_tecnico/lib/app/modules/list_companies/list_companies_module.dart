import 'package:desafio_tecnico/app/modules/list_companies/list_companies_page.dart';
import 'package:desafio_tecnico/app/modules/list_companies/list_companies_controller.dart';
import 'package:desafio_tecnico/app/shared/models/auth_model.dart';
import 'package:desafio_tecnico/app/shared/repositories/companies_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

class ListCompaniesModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => ListCompaniesController(
        companiesRepository: i.get<CompaniesRepository>())),
    Bind((i) =>
        CompaniesRepository(authModel: i.get<AuthModel>(), dio: i.get<Dio>()))
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => const ListCompaniesPage()),
  ];
}
