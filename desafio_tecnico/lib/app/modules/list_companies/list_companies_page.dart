import 'package:desafio_tecnico/app/modules/list_companies/company_details_page.dart';
import 'package:desafio_tecnico/app/modules/list_companies/list_companies_controller.dart';
import 'package:desafio_tecnico/app/modules/list_companies/widgets/company_list_item.dart';
import 'package:desafio_tecnico/app/modules/list_companies/widgets/custom_app_bar.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class ListCompaniesPage extends StatefulWidget {
  final String title;
  const ListCompaniesPage({Key? key, this.title = 'ListCompaniesPage'})
      : super(key: key);
  @override
  ListCompaniesPageState createState() => ListCompaniesPageState();
}

class ListCompaniesPageState
    extends ModularState<ListCompaniesPage, ListCompaniesController> {
  @override
  Widget build(BuildContext context) {
    final isKeyboardVisible = MediaQuery.of(context).viewInsets.bottom > 50;
    var appBarHeight = 60.0;
    if (isKeyboardVisible) {
      appBarHeight = 60.0;
    } else {
      appBarHeight = 200.0;
    }
    return Scaffold(
      appBar: CustomAppBar(height: appBarHeight),
      body: Observer(
        builder: (context) {
          if (controller.initialState) {
            return Container();
          }
          if (controller.busy) {
            return _searchingContentBody();
          }
          if (controller.listedCompanies.isNotEmpty) {
            return _withContentBody(controller);
          }
          return _noContentFoundBody();
        },
      ),
    );
  }
}

Widget _noContentFoundBody() {
  return const Center(
    child: Text(
      "Nenhum resultado encontrado",
      style: TextStyle(color: Color(0xFF666666)),
    ),
  );
}

Widget _withContentBody(ListCompaniesController controller) {
  return Padding(
    padding: const EdgeInsets.only(top: 16),
    child: SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.only(left: 16),
            alignment: Alignment.centerLeft,
            child: Text(
              '${controller.listedCompanies.length} resultados encontrados',
              style: const TextStyle(color: Color(0xFF666666)),
            ),
          ),
          Observer(
            builder: (context) {
              return Column(
                  children: controller.listedCompanies.map((e) {
                final Color color;
                final int index = controller.listedCompanies.indexOf(e);
                if (index % 2 == 0) {
                  color = const Color(0xFF79BBCA);
                } else if (index % 3 == 0) {
                  color = const Color(0xFFEB9797);
                } else {
                  color = const Color(0xFF90BB81);
                }
                return Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Modular.to.push(
                          MaterialPageRoute(
                            builder: (_) => CompanyDetailsPage(
                                companyModel: e, color: color),
                          ),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: CompanyListItem(
                          color: color,
                          companyName: e.name,
                        ),
                      ),
                    )
                  ],
                );
              }).toList());
            },
          ),
        ],
      ),
    ),
  );
}

Widget _searchingContentBody() {
  return const Center(
      child: CircularProgressIndicator(
    color: Color(0xFFFBDBE7),
  ));
}
