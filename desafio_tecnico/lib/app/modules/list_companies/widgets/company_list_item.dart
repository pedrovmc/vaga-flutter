import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CompanyListItem extends StatelessWidget {
  final String companyName;
  final Color color;
  const CompanyListItem({required this.companyName, required this.color});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 120,
        color: color,
        child: Center(
            child: Text(
          companyName.toUpperCase(),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
        )),
      ),
    );
  }
}
