import 'package:desafio_tecnico/app/modules/list_companies/widgets/custom_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  _CustomAppBarState createState() => _CustomAppBarState();
  final double height;
  const CustomAppBar({
    required this.height,
  });

  @override
  Size get preferredSize => Size.fromHeight(height);
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      fit: StackFit.expand,
      children: [
        Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            colors: [Color(0xFF953ABA), Color(0xFFAE197C)],
            begin: Alignment.topCenter,
            end: Alignment.bottomLeft,
          )),
        ),
        Positioned(
          top: widget.height,
          left: 16,
          right: 16,
          child: CustomSearchBar(),
        ),
      ],
    );
  }
}
