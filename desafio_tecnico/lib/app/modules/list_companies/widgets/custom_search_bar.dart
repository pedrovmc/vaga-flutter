import 'package:desafio_tecnico/app/modules/list_companies/list_companies_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CustomSearchBar extends StatelessWidget {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          cursorHeight: 25,
          cursorWidth: 1,
          onChanged: (value) {
            Modular.get<ListCompaniesController>().searchByName(value);
          },
          cursorColor: const Color(0xFFE01E69),
          decoration: const InputDecoration(
              hintText: 'Pesquise por empresa',
              prefixIcon: Icon(
                Icons.search,
                color: Color(0xFF666666),
              ),
              border: InputBorder.none,
              fillColor: Color(0xFFF5F5F5),
              filled: true),
        ),
      ],
    );
  }
}
