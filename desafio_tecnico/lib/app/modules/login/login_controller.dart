import 'package:desafio_tecnico/app/shared/enums/status_enum.dart';
import 'package:desafio_tecnico/app/shared/services/login_service.dart';
import 'package:mobx/mobx.dart';

part 'login_controller.g.dart';

class LoginController = LoginControllerBase with _$LoginController;

abstract class LoginControllerBase with Store {
  final LoginService loginService;
  LoginControllerBase({
    required this.loginService,
  });

  @observable
  Status status = Status.none;

  @action
  Future<void> login(String email, String password) async {
    await loginService.login(email, password);
    status = loginService.authModel.status;
    // return loginService.authModel.status;
  }
}
