import 'package:desafio_tecnico/app/modules/login/login_controller.dart';
import 'package:desafio_tecnico/app/shared/models/auth_model.dart';
import 'package:desafio_tecnico/app/shared/repositories/login_repository.dart';
import 'package:desafio_tecnico/app/shared/services/login_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'login_page.dart';

class LoginModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton(
        (i) => LoginController(loginService: i.get<LoginService>())),
    Bind((i) => LoginService(
          loginRepository: i.get<LoginRepository>(),
          authModel: i.get<AuthModel>(),
        )),
    Bind((i) => LoginRepository(dio: i.get<Dio>()))
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, args) => LoginPage()),
  ];
}
