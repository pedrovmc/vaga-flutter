import 'package:desafio_tecnico/app/modules/login/login_controller.dart';
import 'package:desafio_tecnico/app/modules/login/widgets/custom_login_form_field.dart';
import 'package:desafio_tecnico/app/modules/login/widgets/rounded_app_bar.dart';
import 'package:desafio_tecnico/app/shared/enums/status_enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  @override
  Widget build(BuildContext context) {
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();
    return Scaffold(
      appBar: RoundedAppBar(),
      body: Form(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Observer(
            builder: (context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomLoginFormField(
                    label: 'Email',
                    textEditingcontroller: emailController,
                    status: controller.status,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomLoginFormField(
                    label: 'Senha',
                    textEditingcontroller: passwordController,
                    isPassword: true,
                    status: controller.status,
                  ),
                  if (controller.status == Status.unauthorized)
                    Container(
                      alignment: Alignment.centerRight,
                      child: const Text(
                        'Credenciais incorretas',
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  if (controller.status == Status.timeout)
                    Container(
                      alignment: Alignment.centerRight,
                      child: const Text(
                        'Verifique sua conexão e tente novamente',
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  if (controller.status == Status.serverError)
                    Container(
                      alignment: Alignment.centerRight,
                      child: const Text(
                        'Serviço Indisponível. Tente novamente mais tarde',
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  Container(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: const Color(0xFFE01E69),
                                  minimumSize: const Size.fromHeight(50)),
                              onPressed: () async {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return Container(
                                        color: Colors.black.withOpacity(0.5),
                                        child: const Center(
                                            child: CircularProgressIndicator(
                                          color: Color(0xFFFBDBE7),
                                        )),
                                      );
                                    });
                                await controller.login(emailController.text,
                                    passwordController.text);
                                if (controller.status == Status.success) {
                                  Modular.to
                                      .pushReplacementNamed('/list-companies');
                                } else {
                                  Modular.to.pop();
                                }
                              },
                              child: const Text("ENTRAR")),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 60,
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
