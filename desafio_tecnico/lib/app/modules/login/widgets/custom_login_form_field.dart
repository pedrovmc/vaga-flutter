import 'package:desafio_tecnico/app/shared/enums/status_enum.dart';
import 'package:flutter/material.dart';

class CustomLoginFormField extends StatefulWidget {
  final String label;
  final bool isPassword;
  final TextEditingController textEditingcontroller;
  final Status status;
  const CustomLoginFormField({
    Key? key,
    required this.label,
    required this.textEditingcontroller,
    required this.status,
    this.isPassword = false,
  }) : super(key: key);

  @override
  _CustomLoginFormFieldState createState() => _CustomLoginFormFieldState();
}

class _CustomLoginFormFieldState extends State<CustomLoginFormField> {
  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          child: Text(
            widget.label,
            style: const TextStyle(color: Color(0xFF666666)),
          ),
        ),
        if (widget.status == Status.unauthorized)
          TextFormField(
            controller: widget.textEditingcontroller,
            obscureText: widget.isPassword,
            cursorColor: const Color(0xFFE01E69),
            decoration: const InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 2)),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 2)),
                suffixIcon: Icon(
                  Icons.error,
                  color: Colors.red,
                ),
                border: InputBorder.none,
                fillColor: Color(0xFFF5F5F5),
                filled: true),
          )
        else if (widget.isPassword)
          TextFormField(
            controller: widget.textEditingcontroller,
            obscureText: widget.isPassword && !showPassword,
            cursorColor: const Color(0xFFE01E69),
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: const Color(0xFFF5F5F5),
              filled: true,
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    showPassword = !showPassword;
                  });
                },
                child: showPassword == true
                    ? const Icon(
                        Icons.visibility_off,
                        color: Colors.grey,
                      )
                    : const Icon(
                        Icons.visibility,
                        color: Colors.grey,
                      ),
              ),
            ),
          )
        else
          TextFormField(
            controller: widget.textEditingcontroller,
            obscureText: widget.isPassword && !showPassword,
            cursorColor: const Color(0xFFE01E69),
            decoration: const InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xFFF5F5F5),
              filled: true,
            ),
          )
      ],
    );
  }
}
