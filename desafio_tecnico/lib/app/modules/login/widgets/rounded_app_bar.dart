import 'package:flutter/material.dart';

class RoundedAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    final isKeyboardVisible = MediaQuery.of(context).viewInsets.bottom > 50;

    return Stack(
      children: [
        SizedBox.fromSize(
          size: isKeyboardVisible ? preferredSize / 2 : preferredSize,
          child: LayoutBuilder(builder: (context, constraint) {
            final width = constraint.maxWidth * 8;
            return ClipRect(
              child: OverflowBox(
                maxHeight: double.infinity,
                maxWidth: double.infinity,
                child: SizedBox(
                  width: width,
                  height: width,
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: isKeyboardVisible
                            ? width / 2 - preferredSize.height / 4
                            : width / 2 - preferredSize.height / 2),
                    child: const DecoratedBox(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                "lib/assets/login_appbar_background.png"),
                            fit: BoxFit.cover),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
        ),
        const Positioned.fill(
          child: Align(
            child: Image(
              image: AssetImage('lib/assets/logo_home.png'),
            ),
          ),
        ),
        if (!isKeyboardVisible)
          const Positioned.fill(
            top: 90,
            child: Align(
              child: Text(
                'Seja bem vindo ao empresas!',
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          )
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(250.0);
}
