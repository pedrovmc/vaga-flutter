import 'package:desafio_tecnico/app/shared/enums/status_enum.dart';

class AuthModel {
  String accessToken;
  String client;
  String uid;
  Status status;

  AuthModel({
    required this.accessToken,
    required this.client,
    required this.uid,
    this.status = Status.none,
  });

  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'client': client,
      'uid': uid,
    };
  }

  factory AuthModel.fromMap(Map<String, dynamic> map) {
    return AuthModel(
      accessToken: map['access-token'].first,
      client: map['client'].first,
      uid: map['uid'].first,
    );
  }
}
