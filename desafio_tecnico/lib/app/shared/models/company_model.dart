class CompanyModel {
  final String name;
  final String description;
  CompanyModel({
    required this.name,
    required this.description,
  });

  Map<String, dynamic> toMap() {
    return {
      'enterprise_name': name,
      'description': description,
    };
  }

  factory CompanyModel.fromMap(Map<String, dynamic> map) {
    return CompanyModel(
      name: map['enterprise_name'],
      description: map['description'],
    );
  }
}
