import 'package:desafio_tecnico/app/shared/models/auth_model.dart';
import 'package:desafio_tecnico/app/shared/models/company_model.dart';
import 'package:dio/dio.dart';

class CompaniesRepository {
  final AuthModel authModel;
  final Dio dio;
  CompaniesRepository({required this.authModel, required this.dio});

  Future<List<CompanyModel>> getAllByName(String name) async {
    final response = await dio.get('/enterprises',
        queryParameters: {'name': name},
        options: Options(headers: {
          'access-token': authModel.accessToken,
          'uid': authModel.uid,
          'client': authModel.client
        }));
    final List<Map<String, dynamic>> companiesJson =
        response.data['enterprises'].cast<Map<String, dynamic>>();
    final List<CompanyModel> companies = companiesJson.map((element) {
      final CompanyModel companyModel = CompanyModel(
          name: element['enterprise_name'],
          description: element['description']);
      return companyModel;
    }).toList();
    return companies;
  }
}
