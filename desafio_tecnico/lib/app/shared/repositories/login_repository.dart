import 'package:desafio_tecnico/app/shared/enums/status_enum.dart';
import 'package:desafio_tecnico/app/shared/models/auth_model.dart';
import 'package:dio/dio.dart';

class LoginRepository {
  final Dio dio;
  LoginRepository({required this.dio});

  Future<AuthModel> login(String email, String password) async {
    final Response response;

    try {
      response = await dio.post('/users/auth/sign_in', data: {
        'email': email,
        'password': password,
      });
    } on DioError catch (e) {
      if (e.response == null) {
        return AuthModel(
            status: Status.timeout, accessToken: '', uid: '', client: '');
      }

      final code = e.response!.statusCode;

      if (code == 401) {
        return AuthModel(
            status: Status.unauthorized, accessToken: '', uid: '', client: '');
      } else if (code! >= 500) {
        return AuthModel(
            status: Status.serverError, accessToken: '', uid: '', client: '');
      } else {
        return AuthModel(accessToken: '', uid: '', client: '');
      }
    }

    if (response.data['success']) {
      final AuthModel authModel = AuthModel.fromMap(response.headers.map);
      authModel.status = Status.success;
      return authModel;
    } else {
      return AuthModel(accessToken: '', uid: '', client: '');
    }
  }
}
