import 'package:desafio_tecnico/app/shared/models/auth_model.dart';
import 'package:desafio_tecnico/app/shared/repositories/login_repository.dart';

class LoginService {
  final LoginRepository loginRepository;
  final AuthModel authModel;
  LoginService({
    required this.loginRepository,
    required this.authModel,
  });

  Future<void> login(String email, String password) async {
    final AuthModel response = await loginRepository.login(email, password);
    authModel.status = response.status;
    authModel.uid = response.uid;
    authModel.accessToken = response.accessToken;
    authModel.client = response.client;
  }
}
