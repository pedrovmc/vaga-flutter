import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 2), () {
      Modular.to.pushReplacementNamed('/login');
    });
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: const BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(
                  'lib/assets/splash_bg.png',
                ),
              ),
            ),
          ),
          const Positioned.fill(
            child: Align(
              child: Image(
                image: AssetImage('lib/assets/logo_ioasys.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
