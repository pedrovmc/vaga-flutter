import 'package:flutter_test/flutter_test.dart';
import 'package:desafio_tecnico/app/modules/list_companies/listCompanies_store.dart';
 
void main() {
  late ListCompaniesStore store;

  setUpAll(() {
    store = ListCompaniesStore();
  });

  test('increment count', () async {
    expect(store.value, equals(0));
    store.increment();
    expect(store.value, equals(1));
  });
}